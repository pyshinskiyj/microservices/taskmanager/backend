FROM java:8
COPY ./target/tm-backend.jar /opt/tm-backend.jar
WORKDIR /opt

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "tm-backend.jar"]
