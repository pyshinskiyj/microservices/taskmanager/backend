package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.pyshinskiy.tm.enumerated.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_role")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role extends AbstractEntity {

    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @ManyToOne
    private User user;
}
