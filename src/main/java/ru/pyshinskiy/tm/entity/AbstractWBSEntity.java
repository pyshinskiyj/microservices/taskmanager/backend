package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Cacheable
@MappedSuperclass
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class AbstractWBSEntity extends AbstractEntity {

    @NotNull
    @Basic(optional = false)
    private Date createTime = new Date(System.currentTimeMillis());

    @Basic
    @Nullable
    private String name;

    @Basic
    @Nullable
    private String description;

    @Basic(optional = false)
    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Basic
    @Nullable
    private Date startDate;

    @Basic
    @Nullable
    private Date finishDate;
}
