package ru.pyshinskiy.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
}
