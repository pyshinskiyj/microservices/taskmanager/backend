package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.entity.Project;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @Nullable
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Project findProjectById(@NotNull final String id);

    @NotNull
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("select distinct p from Project p left join fetch p.tasks")
    List<Project> findAllProjectsWithTasks();
}
