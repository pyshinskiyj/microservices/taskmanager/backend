package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.entity.User;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("select distinct u from User u left join fetch u.roles where u.id = :id")
    User findUserById(@Param("id") @NotNull final String id);

    @Nullable
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("select distinct u from User u left join fetch u.roles where u.login = :login")
    User findUserByLogin(@Param("login") @NotNull final String login);

    @NotNull
    @QueryHints(value = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("select distinct u from User u left join fetch u.roles")
    List<User> findAllUsersWithRoles();
}
