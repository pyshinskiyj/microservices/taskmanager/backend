package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public class TaskDTO extends AbstractDTO {

    @Nullable
    private String projectId;

    @NotNull
    private Date createTime = new Date(System.currentTimeMillis());

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Status status = Status.PLANNED;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;
}
