package ru.pyshinskiy.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Role;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.RoleType;
import ru.pyshinskiy.tm.service.project.IProjectService;
import ru.pyshinskiy.tm.service.task.ITaskService;
import ru.pyshinskiy.tm.service.user.IUserService;

import java.util.stream.Collectors;

@Component
public class DTOConverter {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IUserService userService;

    @Nullable
    public ProjectDTO toProjectDTO(@Nullable final Project project) {
        if(project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setStartDate(project.getStartDate());
        projectDTO.setFinishDate(project.getFinishDate());
        projectDTO.setCreateTime(project.getCreateTime());
        return projectDTO;
    }

    @Nullable
    public Project toProject(@Nullable final ProjectDTO projectDTO) {
        if(projectDTO == null) return null;
        @NotNull final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStatus(projectDTO.getStatus());
        project.setStartDate(projectDTO.getStartDate());
        project.setFinishDate(projectDTO.getFinishDate());
        project.setCreateTime(projectDTO.getCreateTime());
        project.setTasks(taskService.findByProject(projectService.findOne(projectDTO.getId())));
        return project;
    }

    @Nullable
    public TaskDTO toTaskDTO(@Nullable final Task task) {
        if(task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        if(task.getProject() != null) {
            taskDTO.setProjectId(task.getProject().getId());
        }
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setStartDate(task.getStartDate());
        taskDTO.setFinishDate(task.getFinishDate());
        taskDTO.setCreateTime(task.getCreateTime());
        return taskDTO;
    }

    @Nullable
    public Task toTask(@Nullable final TaskDTO taskDTO) {
        if(taskDTO == null) return null;
        @NotNull final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setProject(projectService.findOne(taskDTO.getProjectId()));
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setStatus(taskDTO.getStatus());
        task.setStartDate(taskDTO.getStartDate());
        task.setFinishDate(taskDTO.getFinishDate());
        task.setCreateTime(taskDTO.getCreateTime());
        return task;
    }

    @Nullable
    public User toUser(@Nullable final UserDTO userDTO) {
        if(userDTO == null) return null;
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRoles(userDTO.getRoles()
                .stream()
                .map(e -> toRole(e, userDTO.getId()))
                .collect(Collectors.toSet()));
        return user;
    }

    @Nullable
    public UserDTO toUserDTO(@Nullable final User user) {
        if(user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setRoles(user.getRoles()
                .stream()
                .map(e -> e.getRoleType())
                .collect(Collectors.toSet()));
        return userDTO;
    }

    @NotNull
    private Role toRole(@NotNull final RoleType roleType, @NotNull final String userId) {
        @NotNull final Role role = new Role();
        role.setUser(userService.findOne(userId));
        role.setRoleType(roleType);
        return role;
    }
}