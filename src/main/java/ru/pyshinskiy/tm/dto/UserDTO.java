package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.enumerated.RoleType;

import java.util.Set;

@Getter
@Setter
public class UserDTO extends AbstractDTO {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private Set<RoleType> roles;
}
