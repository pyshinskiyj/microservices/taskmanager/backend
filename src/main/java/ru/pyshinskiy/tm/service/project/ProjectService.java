package ru.pyshinskiy.tm.service.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Nullable
    public Project findOne(@Nullable final String id){
        if(id == null || id.isEmpty()) return null;
        return projectRepository.findProjectById(id);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @NotNull
    public List<Project> findAllProjectsWithTasks() {
        return projectRepository.findAllProjectsWithTasks();
    }

    @Transactional
    @Override
    public Project save(@Nullable final Project project) {
        return projectRepository.save(project);
    }

    @Transactional
    @Override
    public void remove(@Nullable final String projectId) {
        projectRepository.deleteById(projectId);
    }

}
