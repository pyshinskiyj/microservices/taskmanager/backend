package ru.pyshinskiy.tm.service.project;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.service.IService;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAllProjectsWithTasks();
}
