package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @Nullable
    T findOne(@Nullable final String id);

    @NotNull
    List<T> findAll();

    T save(@Nullable final T t);

    void remove(@Nullable final String id);
}
