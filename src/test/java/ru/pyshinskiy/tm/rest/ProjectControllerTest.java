package ru.pyshinskiy.tm.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.enumerated.Status;

import java.util.Date;
import java.util.Random;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProjectControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = getProjectDTO();
        mockMvc.perform(post("/api/project/create")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(projectDTO))
        )
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(projectDTO)));
    }

    @Test
    public void findProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = getProjectDTO();
        mockMvc.perform(post("/api/project/create")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(projectDTO))
        )
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(projectDTO)));
        mockMvc.perform(get("/api/project/" + projectDTO.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(projectDTO)));
    }

    @Test
    public void updateProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = getProjectDTO();
        mockMvc.perform(post("/api/project/create")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(projectDTO))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE));
        projectDTO.setName(new Random().nextInt() + "");
        mockMvc.perform(put("/api/project/update")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(projectDTO))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(projectDTO)));
    }

    @Test
    public void deleteProject() throws Exception {
        @NotNull final ProjectDTO projectDTO = getProjectDTO();
        mockMvc.perform(post("/api/project/create")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(projectDTO))
        )
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(projectDTO)));

        mockMvc.perform(delete("/api/project/delete/" + projectDTO.getId()))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/project/" + projectDTO.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    private ProjectDTO getProjectDTO() {
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(new Random().nextInt() + "");
        projectDTO.setDescription(new Random().nextInt() + "");
        projectDTO.setStatus(Status.IN_PROGRESS);
        projectDTO.setCreateTime(new Date());
        projectDTO.setStartDate(new Date());
        projectDTO.setFinishDate(new Date());
        return projectDTO;
    }
}
